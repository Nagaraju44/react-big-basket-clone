import initialState from "../initState/initState"

function reducer(state = initialState, action) {
    switch (action.type) {
        case "ADD_PRODUCT_TO_CART":
            let isPresentPrev = state.cartItems.filter((item) => {
                return action.payload.id == item.id
            })
            if (isPresentPrev.length > 0) {
                return state
            } else {
                let cartItemSelected = state.ProductsData.filter((product) => {
                    return action.payload.id == product.id
                })
                return { ...state, cartItems: [...state.cartItems, { ...cartItemSelected[0], number: 1 }] }
            }

        case "INCREASE_QUANTITY":
            let increasedProduct = state.cartItems.map((item) => {
                if (item.id == action.payload.id) {
                    let number = item.number + 1
                    return { ...item, number: number }
                }
                return item
            })
            return { ...state, cartItems: increasedProduct }

        case "DECREASE_QUANTITY":
            let decreasedProduct = state.cartItems.map((item) => {
                if (item.id == action.payload.id) {
                    let number = item.number - 1
                    if (number > 0) {
                        return { ...item, number: number }
                    }
                    return item
                }
                return item
            })
            return { ...state, cartItems: decreasedProduct }
        case "REMOVE_ITEM_FROM_CART":
            let remainItems = state.cartItems.filter((item) => {
                return item.id != action.payload.id
            })
            return { ...state, cartItems: remainItems }

        case "ADD_USER_INFO":
            return { ...state, UserInfo: [...state.UserInfo, action.payload] }

        case "ADD_USER_ADDRESS":
            return { ...state, UserAddress: [action.payload] }
        
        case "REMOVE_ADDRESS":
            return {...state, UserAddress: []}

        case "ADD_PRODUCTS_TO_ORDERS":
            return {...state, orderedItems:[...state.orderedItems, ...action.payload.products]}

        case "EMPTY_CART":
            return {...state, cartItems:[]}

        case "ADD_ITEMS_TO_HISTORY":
            return {...state, orderedHistory:[...state.orderedHistory, ...action.payload.products]}

        case "EMPTY_ORDERED_ITEMS":
            return {...state, orderedItems:[]}
            
        default:
            return state
    }
}

export default reducer

import React, { Component } from 'react'
import initialState from '../../Redux/initState/initState'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { addProductToCart } from '../../Redux/Actions/Actions'
import { IncreaseQuantity } from '../../Redux/Actions/Actions'
import { DecreaseQuantity } from '../../Redux/Actions/Actions'
import { RemoveItemFromCart } from '../../Redux/Actions/Actions'

export class ViewProduct extends Component {
    state = { Id: window.location.pathname.split('/')[2], isAddedToCart: false }

    FuncIncreaseQuantity = (event) =>{
        this.props.IncreaseQuantity(event.target.id)
    }

    FuncDecreaseQuantity = (event) =>{
        let requiredItem = this.props.cartItems.filter((item)=>{
            return item.id == event.target.id
        })[0]

        if (requiredItem.number > 1){
            this.props.DecreaseQuantity(event.target.id)
        } else{
            this.setState({ isAddedToCart: false})
            this.props.RemoveItemFromCart(event.target.id)
        }
    }

    AddingProductToCart = (event) => {
        this.setState({ isAddedToCart: true })
        this.props.addProductToCart(event.target.id)
    }

    getQuantity = (id) =>{
        let product = this.props.cartItems.filter((item)=>{
            return item.id == id
        })
        return product[0].number
    }

    render() {
        const requireProduct = initialState.ProductsData.filter((product) => {
            return product.id == this.state.Id
        })[0]
        
        let keyValue = 0
        let keyIngr = 0

        return (
            <div className='container'>
                <div className='row mt-5'>
                    <div className='col-12 text-start'>
                        <Link to='/displayproducts'><i className="fa-solid fa-arrow-left mb-5 fs-3 text-dark"></i></Link>
                    </div>
                    <div className='col-lg-6 col-md-10 col-12 ps-lg-5 ms-lg-4'>
                        <img src={requireProduct.img} alt='productImage' className='w-75' />
                    </div>
                    <div className='col-lg-4 col-12 pt-5 text-start ps-lg-0 ps-5'>
                        <h4 className='text-wrap text-bold'><strong>{requireProduct.title}</strong></h4>
                        <p >Category: {requireProduct.category}</p>
                        <h6>Qty: {requireProduct.quantity}</h6>
                        <p className='mb-0 fs-6 text-secondary'>MRP:<s className='fs-6'>Rs.{requireProduct.originalPrice}</s></p>
                        <p><strong>Price:{requireProduct.price}/-</strong><span className='text-danger'> ({requireProduct.discount}% Offer)</span></p>
                        <p className='text-success w-25 rounded text-center py-1' style={{ backgroundColor: '#e4ebdb' }}>
                            <strong>{requireProduct.rating.rate} <i className="bi bi-star-fill"></i></strong>
                        </p>
                        <p><ins>{requireProduct.rating.count} ratings</ins></p>
                        {this.state.isAddedToCart === false ?
                            <button className='btn btn-danger w-50 mb-2' id={`${requireProduct.id}`} onClick={this.AddingProductToCart} >ADD CART<i className="fa-solid fa-basket-shopping ms-2" id={`${requireProduct.id}`} onClick={this.AddingProductToCart}></i></button>
                            :
                            <div className='d-flex'>
                                <button className='btn btn-danger px-4 me-3 h-50 rounded-0' id={`${requireProduct.id}`} onClick={this.FuncDecreaseQuantity}>-</button>
                                <p className='fs-3'> {this.getQuantity(requireProduct.id) ? this.getQuantity(requireProduct.id) : 1} in cart</p>
                                <button className='btn btn-success px-4 ms-3 h-50 rounded-0' id={`${requireProduct.id}`} onClick={this.FuncIncreaseQuantity}>+</button>
                            </div>}
                        <p>Delivers in 24Hrs</p>
                    </div>
                    <div className='col-12 text-start mt-3'>
                        <h4><strong>About Product</strong></h4>
                        <p className='text-wrap'>{requireProduct.body}</p>
                        <hr />
                        <h4><strong>Ingredients</strong></h4>
                        <ul>
                            {requireProduct.Ingredients.map((Ingredient) => {
                                return <li key={++keyIngr}>{Ingredient}</li>
                            })}
                        </ul>
                        <hr />
                        <h4><strong>Nutritional Facts</strong></h4>
                        <ul>
                            {requireProduct.Nutrition.map((Nutrient) => {
                                return <li key={++keyValue}>{Nutrient}</li>
                            })}
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        ProductsData: state.ProductsData,
        cartItems: state.cartItems
    }
}

const mapDispatchToProps = {
    addProductToCart,
    IncreaseQuantity,
    DecreaseQuantity,
    RemoveItemFromCart
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewProduct)
import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export class Courosel extends Component {
    render() {
        return (
            <div className='container-fluid'>
                <div className='row'>
                    <div className='col-lg-12'>
                        <Link to='/displayproducts'>
                            <div id="carouselExampleIndicators" className="carousel slide" data-bs-ride="true">
                                <div className="carousel-indicators">
                                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
                                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="3" aria-label="Slide 4"></button>
                                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="4" aria-label="Slide 5"></button>
                                </div>
                                <div className="carousel-inner">
                                    <div className="carousel-item active">
                                        <img src="https://www.bigbasket.com/media/uploads/banner_images/YXHP144_hp_fom_m_bbpl-staples_800_050922_Bangalore.jpg" className="d-block w-100" alt="image1" />
                                    </div>
                                    <div className="carousel-item">
                                        <img src="https://www.bigbasket.com/media/uploads/banner_images/HP_EMF_M_T1-1600x460_220708.jpg" className="d-block w-100" alt="image2" />
                                    </div>
                                    <div className="carousel-item">
                                        <img src="https://www.bigbasket.com/media/uploads/banner_images/fresho-days-fnv-Ahmedabad-Gandhinagar-1600x460-06thSEP22.jpeg" className="d-block w-100" alt="image3" />
                                    </div>
                                    <div className="carousel-item">
                                        <img src="https://www.bigbasket.com/media/uploads/banner_images/hp_m_premium-tender-coconut_1600x460px-030922.jpg" className="d-block w-100" alt="image4" />
                                    </div>
                                    <div className="carousel-item">
                                        <img src="https://www.bigbasket.com/media/uploads/banner_images/DT_HP_YXNP4896_1600x460x010922.jpg" className="d-block w-100" alt="image5" />
                                    </div>
                                </div>
                                <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span className="visually-hidden">Previous</span>
                                </button>
                                <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span className="visually-hidden">Next</span>
                                </button>
                            </div>
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}

export default Courosel
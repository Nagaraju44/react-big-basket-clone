import React, { Component } from 'react'
import validator from 'validator'
import { AddUserAddress } from '../../Redux/Actions/Actions'
import { RemoveAddress } from '../../Redux/Actions/Actions'
import { AddProductsToOrders } from '../../Redux/Actions/Actions'
import { AddItemsToHistory } from '../../Redux/Actions/Actions'
import { EmptyCart } from '../../Redux/Actions/Actions'
import { connect } from 'react-redux'
import { v4 as uuidv4 } from 'uuid'
import { Link } from 'react-router-dom'

export class Checkout extends Component {
    state = {
        isAddressAdded: false,
        FirstName: "",
        LastName: "",
        PhoneNumber: "",
        Email: "",
        Address: "",
        City: "",
        State: "Choose...",
        PinCode: "",
        creditcard: "",
        validthru: "",
        cvv: "",
        UPIid: "",
        FirstNameErr: "",
        LastNameErr: "",
        PhoneNumberErr: "",
        AddressErr: "",
        CityErr: "",
        StateErr: "",
        PinCodeErr: "",
        EmailErr: "",
        CreditcardErr: "",
        ValidthruErr: "",
        CVVErr: "",
        UPIidErr: "",
        isFirstNameValid: false,
        isLastNameValid: false,
        isPhoneNumberValid: false,
        isEmailValid: false,
        isAddressValid: false,
        isCityValid: false,
        isStateValid: false,
        isPinCodeValid: false,
        isCreditcardValid: false,
        isValidthruValid: false,
        isCVVvalid: false,
        isUPIvalid: false,
        isSuccessed: false,
        isPaymentSuccessed: false
    }

    EnteringDetails = (event) => {
        this.setState({ [event.target.id]: event.target.value })
    }

    addingAddressAfterValidation = () => {
        const { FirstName, LastName, PhoneNumber, Email, Address, City, State, PinCode } = this.state
        if ((this.state.isFirstNameValid === true) &&
            (this.state.isLastNameValid === true) &&
            (this.state.isPhoneNumberValid === true) &&
            (this.state.isEmailValid === true) &&
            (this.state.isAddressValid === true) &&
            (this.state.isCityValid === true) &&
            (this.state.isStateValid === true) &&
            (this.state.isPinCodeValid === true)) {
            this.props.AddUserAddress(FirstName, LastName, PhoneNumber, Email, Address, City, State, PinCode)
        }

        if (this.state.isSuccessed === true) {
            this.setState({ isAddressAdded: true })
        } else {
            this.setState({ isAddressAdded: false })
        }
    }

    SignUpValidation = (event) => {
        event.preventDefault()
        const { FirstName, LastName, PhoneNumber, Email, Address, City, State, PinCode } = this.state
        if ((FirstName === "") || (validator.isAlpha(FirstName) === false)) {
            var FirstNameError = "*Enter Valid First Name"
            var FnameValid = false
        } else {
            var FirstNameError = ""
            var FnameValid = true
        }

        if ((LastName === "") || (validator.isAlpha(LastName) === false)) {
            var LastNameError = "*Enter Valid Last Name"
            var LnameValid = false
        } else {
            var LastNameError = ""
            var LnameValid = true
        }

        if ((PhoneNumber === "") || (validator.isMobilePhone(PhoneNumber) === false) || (PhoneNumber.length != 10) || ((!['9', '8', '7', '6'].includes(PhoneNumber[0])))) {
            var PhoneNumberError = "*Enter Valid Phone Number"
            var PhoneNumberValid = false
        } else {
            var PhoneNumberError = ""
            var PhoneNumberValid = true
        }

        if ((Email === "") || (validator.isEmail(Email) === false)) {
            var EmailError = "*Enter Valid Email"
            var EmailValid = false
        } else {
            var EmailError = ""
            var EmailValid = true
        }

        if (Address === "") {
            var AddressError = "*Enter Address"
            var AddressValid = false
        } else {
            var AddressError = ""
            var AddressValid = true
        }

        if ((City === "") || (validator.isAlpha(City) === false)) {
            var CityError = "*Enter Valid City Name"
            var CitynameValid = false
        } else {
            var CityError = ""
            var CitynameValid = true
        }

        if (State === "Choose...") {
            var StateError = "*Please Select State"
            var StateValid = false
        } else {
            var StateError = ""
            var StateValid = true
        }

        if ((PinCode === "") || (validator.isMobilePhone(PinCode) === false) || (PinCode.length != 6)) {
            var PinCodeError = "*Enter Valid PinCode"
            var PinCodeValid = false
        } else {
            var PinCodeError = ""
            var PinCodeValid = true
        }


        if (FnameValid && LnameValid && PhoneNumberValid && EmailValid && AddressValid && CitynameValid && StateValid && PinCodeValid) {
            var isSuccess = true
        } else {
            var isSuccess = false
        }

        this.setState({
            FirstNameErr: FirstNameError,
            LastNameErr: LastNameError,
            PhoneNumberErr: PhoneNumberError,
            EmailErr: EmailError,
            AddressErr: AddressError,
            CityErr: CityError,
            StateErr: StateError,
            PinCodeErr: PinCodeError,
            isFirstNameValid: FnameValid,
            isLastNameValid: LnameValid,
            isPhoneNumberValid: PhoneNumberValid,
            isEmailValid: EmailValid,
            isAddressValid: AddressValid,
            isCityValid: CitynameValid,
            isStateValid: StateValid,
            isPinCodeValid: PinCodeValid,
            isSuccessed: isSuccess
        }, () => {
            this.addingAddressAfterValidation()
        })
    }

    PaymentValidation = () => {
        const { creditcard, validthru, cvv } = this.state
        if ((creditcard === "") || (validator.isCreditCard(creditcard) === false)) {
            var creditcardError = "*Please Enter Valid Card Name"
            var creditcardvalidation = false
        } else {
            var creditcardError = ""
            var creditcardvalidation = true
        }

        if ((validthru === "") || (/^(0[1-9]|1[0-2])\/?([0-9]{4}|[0-9]{2})$/.test(validthru) === false)) {
            var validthruError = "*Please Enter Valid Date"
            var validthruvalidation = false
        } else {
            var validthruError = ""
            var validthruvalidation = true
        }

        if ((cvv === "") || (validator.isInt(cvv) === false) || (cvv.length !== 3)) {
            var cvvError = "*Please Enter Valid Card Name"
            var cvvValidation = false
        } else {
            var cvvError = ""
            var cvvValidation = true
        }

        if (creditcardvalidation && validthruvalidation && cvvValidation) {
            var isPayable = true
        } else {
            var isPayable = false
        }

        this.setState({
            CreditcardErr: creditcardError,
            ValidthruErr: validthruError,
            CVVErr: cvvError,
            isCreditcardValid: creditcardvalidation,
            isValidthruValid: validthruvalidation,
            isCVVvalid: cvvValidation,
            isPaymentSuccessed: isPayable
        })

    }

    UPIValidation = () => {
        const { UPIid } = this.state
        if (UPIid === "") {
            var UPIidError = "*Please Enter Valid UPI"
            var UPIValid = false
        } else {
            if ((UPIid[10] != '@') || (validator.isMobilePhone(UPIid.slice(0, 10)) === false) || ((!['9', '8', '7', '6'].includes(UPIid.slice(0, 10)[0]))) || (UPIid.length != 14)) {
                var UPIidError = "*Please Enter Valid UPI"
                var UPIValid = false
            } else {
                var UPIidError = ""
                var UPIValid = true
            }
        }

        if (UPIValid) {
            var isPayable = true
        } else {
            var isPayable = false
        }

        this.setState({
            UPIidErr: UPIidError,
            isUPIvalid: UPIValid,
            isPaymentSuccessed: isPayable
        })
    }

    ChangeAddress = ()=>{
        this.props.RemoveAddress()
    }

    AddItemsToOrders = ()=>{
        this.props.AddProductsToOrders(this.props.cartItems)
        this.props.AddItemsToHistory(this.props.cartItems)
        this.props.EmptyCart()
    }

    render() {
        if ((this.props.UserAddress.length > 0)) {
            return <div className='container'>
                <div className='row'>
                    <div className='col-12 d-flex text-start mt-5'>
                        <h4><strong>Your Address<i className="bi bi-geo-alt-fill text-danger"></i></strong></h4>
                        <button className='btn btn-primary ms-auto' onClick={this.ChangeAddress}>CHANGE ADDRESS</button>
                    </div>
                    <div className='col-12 text-start'>
                        <hr />
                        {this.props.UserAddress.map((address) => {
                            return <div key={uuidv4()}>
                                <h4><strong>{address.Name}</strong></h4>
                                <p className='my-0'><strong>Address: </strong>{address.Address}</p>
                                <p className='my-0'>{address.City}, {address.State}</p>
                                <p className='my-0'>{address.PinCode}</p>
                                <p className='my-0'>{address.Mobile}</p>
                                <p>{address.Email}</p>
                            </div>
                        })}
                    </div>
                    <div className='col-12 text-start mt-4'>
                        <h4><strong><i className="bi bi-credit-card-fill me-2 text-primary"></i>Select Payment Method</strong></h4>
                        <hr />
                        <p>
                            <button className="border border-start-0 border-top-0 border-end-0 border-secondary border-opacity-50" style={{ backgroundColor: '#fff' }} type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                Credit / Debit Card
                            </button>
                        </p>
                        <div className="collapse" id="collapseExample">
                            <div className="card card-body">
                                <div className='w-25 ms-auto'>
                                    <img src='https://www.freepnglogos.com/uploads/visa-and-mastercard-logo-26.png' alt='payment' className='w-100 me-2' />
                                </div>
                                <label htmlFor='creditcard'>Credit/Debit Card</label>
                                <input type="text" className="form-control w-75 border border-start-0 border-top-0 border-end-0 border-secondary border-opacity-50 rounded-0" id="creditcard" aria-describedby="textHelp" onChange={this.EnteringDetails} />
                                <span className='text-danger'> {this.state.CreditcardErr === "" ? <span>&nbsp;</span> : this.state.CreditcardErr}</span>
                                <label htmlFor='validthru' className='mt-4'>Valid Thru</label>
                                <input type="text" className="form-control w-25 border border-start-0 border-top-0 border-end-0 border-secondary border-opacity-50 rounded-0" id="validthru" aria-describedby="textHelp" onChange={this.EnteringDetails} />
                                <span className='text-danger'> {this.state.ValidthruErr === "" ? <span>&nbsp;</span> : this.state.ValidthruErr}</span>
                                <label htmlFor='cvv' className='mt-4'>CVV</label>
                                <input type="password" className="form-control w-25 border border-start-0 border-top-0 border-end-0 border-secondary border-opacity-50 rounded-0" id="cvv" aria-describedby="textHelp" onChange={this.EnteringDetails} />
                                <span className='text-danger'> {this.state.CVVErr === "" ? <span>&nbsp;</span> : this.state.CVVErr}</span>

                                <button className='btn btn-success btn-sm mt-5 w-25' onClick={this.PaymentValidation}>Proceed</button>

                            </div>
                        </div>

                        <p>
                            <button className="border border-start-0 border-top-0 border-end-0 border-secondary border-opacity-50" style={{ backgroundColor: '#fff' }} type="button" data-bs-toggle="collapse" data-bs-target="#UPIExample" aria-expanded="false" aria-controls="collapseExample">
                                UPI
                            </button>
                        </p>
                        <div className="collapse" id="UPIExample">
                            <div className="card card-body">
                                <label htmlFor='UPIid'>UPI ID</label>
                                <input type="text" placeholder='1234567890@upi' className="form-control w-75 border border-start-0 border-top-0 border-end-0 border-secondary rounded-0 border-opacity-50" id="UPIid" aria-describedby="textHelp" onChange={this.EnteringDetails} />
                                <span className='text-danger'> {this.state.UPIidErr === "" ? <span>&nbsp;</span> : this.state.UPIidErr}</span>

                                <button className='btn btn-success btn-sm mt-3 w-25' onClick={this.UPIValidation}>Proceed</button>

                                <p className='text-center mt-3'>OR</p>
                                <div className='d-flex w-25 mx-auto justify-content-center'>
                                    <img className='w-25 rounded-circle border border-secondary me-2' src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQmsqtTJM91-GFB5yFcBozPiya-gkXm5v7HKg&usqp=CAU' alt='payment' />
                                    <img className='w-25 rounded-circle border border-secondary me-2' src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTckJJRo9jOQywmhPZBXx9Z_HGyKiHPmNvYwCw5jM_9EL1r0YqjJbAg6iBanBAbDqq8MiM&usqp=CAU' alt='payment' />
                                    <img className='w-25 rounded-circle border border-secondary me-2' src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQp4FbqRxCp6l4W0dqRFGkBiHkvepHyh_F2FBY72lIBRwX3PSlEU20ij3xssI8k4HR-Xz0&usqp=CAU' alt='payment' />
                                </div>
                            </div>
                        </div>

                        <p>
                            <button className="border border-start-0 border-top-0 border-end-0 border-secondary border-opacity-50" style={{ backgroundColor: '#fff' }} type="button" data-bs-toggle="collapse" data-bs-target="#cashExample" aria-expanded="false" aria-controls="collapseExample">
                                Cash On Delivery
                            </button>
                        </p>
                        <div className="collapse" id="cashExample">
                            <div className="card card-body">
                                <p>Pay With Cash When Product Delivered</p>
                                <Link to='/orderStatus'>
                                    <button className='btn btn-success btn-sm mt-3 w-25' onClick={this.AddItemsToOrders}>Proceed</button>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        }
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-12 text-start mt-5'>
                        <h4><strong>Add Address<i className="bi bi-geo-alt-fill text-danger"></i></strong></h4>
                        <hr />
                    </div>
                    <div className='col-12 text-start'>
                        <form className="row g-3">
                            <div className="col-md-6">
                                <label htmlFor="FirstName" className="form-label">FirstName</label>
                                <input type="text" className="form-control" id="FirstName" onChange={this.EnteringDetails} />
                                <span className='text-danger'> {this.state.FirstNameErr === "" ? <span>&nbsp;</span> : this.state.FirstNameErr}</span>
                            </div>
                            <div className="col-md-6">
                                <label htmlFor="LastName" className="form-label">LastName</label>
                                <input type="text" className="form-control" id="LastName" onChange={this.EnteringDetails} />
                                <span className='text-danger'> {this.state.LastNameErr === "" ? <span>&nbsp;</span> : this.state.LastNameErr}</span>
                            </div>
                            <div className="col-md-6">
                                <label htmlFor="Email" className="form-label">Email</label>
                                <input type="email" className="form-control" id="Email" onChange={this.EnteringDetails} />
                                <span className='text-danger'> {this.state.EmailErr === "" ? <span>&nbsp;</span> : this.state.EmailErr}</span>
                            </div>
                            <div className="col-md-6">
                                <label htmlFor="PhoneNumber" className="form-label">PhoneNumber</label>
                                <input type="text" className="form-control" id="PhoneNumber" onChange={this.EnteringDetails} />
                                <span className='text-danger'> {this.state.PhoneNumberErr === "" ? <span>&nbsp;</span> : this.state.PhoneNumberErr}</span>
                            </div>
                            <div className="col-12">
                                <label htmlFor="Address" className="form-label">Address</label>
                                <input type="text" className="form-control" id="Address" placeholder="1234 Main St" onChange={this.EnteringDetails} />
                                <span className='text-danger'> {this.state.AddressErr === "" ? <span>&nbsp;</span> : this.state.AddressErr}</span>
                            </div>
                            <div className="col-md-6">
                                <label htmlFor="City" className="form-label">City</label>
                                <input type="text" className="form-control" id="City" onChange={this.EnteringDetails} />
                                <span className='text-danger'> {this.state.CityErr === "" ? <span>&nbsp;</span> : this.state.CityErr}</span>
                            </div>
                            <div className="col-md-4">
                                <label htmlFor="State" className="form-label">State</label>
                                <select id="State" className="form-select" onChange={this.EnteringDetails} >
                                    <option defaultValue>Choose...</option>
                                    <option>Andhra Pradesh</option>
                                    <option>Arunachal Pradesh</option>
                                    <option>Assam</option>
                                    <option>Bihar</option>
                                    <option>Chhattisgarh</option>
                                    <option>Goa</option>
                                    <option>Gujarat</option>
                                    <option>Haryana</option>
                                    <option>Himachal Pradesh</option>
                                    <option>Jharkhand</option>
                                    <option>Karnataka</option>
                                    <option>Kerala</option>
                                    <option>Madhya Pradesh</option>
                                    <option>Maharashtra</option>
                                    <option>Manipur</option>
                                    <option>Meghalaya</option>
                                    <option>Mizoram</option>
                                    <option>Nagaland</option>
                                    <option>Odisha</option>
                                    <option>Punjab</option>
                                    <option>Rajasthan</option>
                                    <option>Sikkim</option>
                                    <option>Tamil Nadu</option>
                                    <option>Telangana</option>
                                    <option>Tripura</option>
                                    <option>Uttarakhand</option>
                                    <option>Uttar Pradesh</option>
                                    <option>West Bengal</option>
                                </select>
                                <span className='text-danger'> {this.state.StateErr === "" ? <span>&nbsp;</span> : this.state.StateErr}</span>
                            </div>
                            <div className="col-md-2">
                                <label htmlFor="PinCode" className="form-label">PinCode</label>
                                <input type="text" className="form-control" id="PinCode" onChange={this.EnteringDetails} />
                                <span className='text-danger'> {this.state.PinCodeErr === "" ? <span>&nbsp;</span> : this.state.PinCodeErr}</span>
                            </div>
                            <div className="col-12">
                                <button type="submit" className="btn btn-primary" onClick={this.SignUpValidation}>Add</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        UserAddress: state.UserAddress,
        cartItems: state.cartItems
    }
}

const mapDispatchToProps = {
    AddUserAddress,
    RemoveAddress,
    AddProductsToOrders,
    EmptyCart,
    AddItemsToHistory
}



export default connect(mapStateToProps, mapDispatchToProps)(Checkout)


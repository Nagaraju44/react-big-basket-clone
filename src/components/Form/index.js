import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { AddUserInfo } from '../../Redux/Actions/Actions'
import validator from 'validator'

class Form extends Component {
  state = {
    FirstName: "",
    LastName: "",
    PhoneNumber: "",
    Email: "",
    FirstNameErr: "",
    LastNameErr: "",
    PhoneNumberErr: "",
    EmailErr: "",
    isFirstNameValid: false,
    isLastNameValid: false,
    isPhoneNumberValid: false,
    isEmailValid: false,
    isSuccessed: false
  }

  EnteringDetails = (event) => {
    this.setState({ [event.target.id]: event.target.value })
  }

  addingUserAfterValidation = () => {
    const { FirstName, LastName, PhoneNumber, Email } = this.state
    if ((this.state.isFirstNameValid === true) &&
      (this.state.isLastNameValid === true) &&
      (this.state.isPhoneNumberValid === true) &&
      (this.state.isEmailValid === true)) {
      this.props.AddUserInfo(FirstName, LastName, PhoneNumber, Email)
    }
  }

  SignUpValidation = (event) => {
    event.preventDefault()
    const { FirstName, LastName, PhoneNumber, Email } = this.state
    if ((FirstName === "") || (validator.isAlpha(FirstName) === false)) {
      var FirstNameError = "*Enter Valid First Name"
      var FnameValid = false
    } else {
      var FirstNameError = ""
      var FnameValid = true
    }

    if ((LastName === "") || (validator.isAlpha(LastName) === false)) {
      var LastNameError = "*Enter Valid Last Name"
      var LnameValid = false
    } else {
      var LastNameError = ""
      var LnameValid = true
    }

    if ((PhoneNumber === "") || (validator.isMobilePhone(PhoneNumber) === false) || (PhoneNumber.length != 10) || ((!['9', '8', '7', '6'].includes(PhoneNumber[0])))) {
      var PhoneNumberError = "*Enter Valid Phone Number"
      var PhoneNumberValid = false
    } else {
      var PhoneNumberError = ""
      var PhoneNumberValid = true
    }

    if ((Email === "") || (validator.isEmail(Email) === false)) {
      var EmailError = "*Enter Valid Email"
      var EmailValid = false
    } else {
      var EmailError = ""
      var EmailValid = true
    }

    if (FnameValid && LnameValid && PhoneNumberValid && EmailValid) {
      var isSuccess = true
    } else {
      var isSuccess = false
    }

    this.setState({
      FirstNameErr: FirstNameError,
      LastNameErr: LastNameError,
      PhoneNumberErr: PhoneNumberError,
      EmailErr: EmailError,
      isFirstNameValid: FnameValid,
      isLastNameValid: LnameValid,
      isPhoneNumberValid: PhoneNumberValid,
      isEmailValid: EmailValid,
      isSuccessed: isSuccess
    }, () => {
      this.addingUserAfterValidation()
    })
  }

  render() {
    return (
      <div className="container mt-5 text-start">
        <h3><strong>Please Sign Up</strong></h3>
        <form>
          <div className="mb-1">
            <label htmlFor="FirstName" className="form-label">FirstName</label>
            <input type="text" placeholder='Ex: Robert' className="form-control" id="FirstName" aria-describedby="textHelp" onChange={this.EnteringDetails} />
            <span className='text-danger'> {this.state.FirstNameErr === "" ? <span>&nbsp;</span> : this.state.FirstNameErr}</span>
          </div>

          <div className="mb-1">
            <label htmlFor="LastName" className="form-label">LastName</label>
            <input type="text" placeholder='Ex: DowneyJr' className="form-control" id="LastName" aria-describedby="textHelp" onChange={this.EnteringDetails} />
            <span className='text-danger'> {this.state.LastNameErr === "" ? <span>&nbsp;</span> : this.state.LastNameErr}</span>
          </div>

          <div className="mb-1">
            <label htmlFor="PhoneNumber" className="form-label">Phone Number</label>
            <input type="text" placeholder='Ex: 96******64' className="form-control" id="PhoneNumber" aria-describedby="numberHelp" onChange={this.EnteringDetails} />
            <span className='text-danger'> {this.state.PhoneNumberErr === "" ? <span>&nbsp;</span> : this.state.PhoneNumberErr}</span>
          </div>

          <div className="mb-1">
            <label htmlFor="Email" className="form-label">Email</label>
            <input type="email" placeholder='Ex: Robert@gmail.com' className="form-control" id="Email" aria-describedby="emailHelp" onChange={this.EnteringDetails} />
            <span className='text-danger'> {this.state.EmailErr === "" ? <span>&nbsp;</span> : this.state.EmailErr}</span>
          </div>

          {this.state.isSuccessed === true ?
            <div>
              <h4 className='text-success'>Thank You For Register. Click Below to CheckOut</h4>
              <Link to='/checkout'>
                <button className="btn btn-success" >Continue</button>
              </Link>
            </div>
            :
            <div>
              <h4>&nbsp;</h4>
              <button type="submit" className="btn btn-primary" onClick={this.SignUpValidation}>Submit</button>
            </div>} <br />
        </form>

      </div>)

  }
}

const mapDispatchToProps = {
  AddUserInfo
}

export default connect(null, mapDispatchToProps)(Form)

import React, { Component } from 'react'

export class MoreProducts extends Component {
    render() {
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-lg-12 ps-0 mt-5'>
                        <h3 className='text-dark'>Top Offers</h3>
                        <hr />
                        <div className='row'>
                            <div className='col-lg-3  col-6 mb-2'>
                            <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/d40b0cdb-e07b-4873-ad11-b9877576ebaf/hp_deals-of-the-weeks-topoffersStorefront_m_480_250822_01.jpg' alt="Deal" className='w-100 ms-0 me-2 border border-secondary border-opacity-25' />
                            </div>
                            <div className='col-lg-3 col-6 mb-2'>
                            <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/d40b0cdb-e07b-4873-ad11-b9877576ebaf/hp_bg-pack-biger-topoffersStorefront_m_480_250822_02.jpg' alt='deal' className='w-100 me-2 border border-secondary border-opacity-25' />
                            </div>
                            <div className='col-lg-3 col-6 mb-2'>
                            <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/d40b0cdb-e07b-4873-ad11-b9877576ebaf/hp_combos-you-cant-miss-topoffersStorefront_m_480_250822_03.jpg' alt='deal' className='w-100 me-2 border border-secondary border-opacity-25' />
                            </div>
                            <div className='col-lg-3 col-6 mb-2'>
                            <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/d40b0cdb-e07b-4873-ad11-b9877576ebaf/hp_30-corner-topoffersStorefront_m_480_250822_04.jpg' alt='deal' className='w-100 me-2 border border-secondary border-opacity-25' />
                            </div>
                        </div>
                    </div>
                    <div className='col-12 mt-5'>
                        <h3 className='text-dark'>Fruits & Vegetables</h3>
                        <hr />
                    </div>
                    <div className='col-lg-6 col-12 '>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/e1a00954-68f4-4295-a01a-2319ef66f039/hp_orhanice-fnv-fnvStorefront_m_250822_560x378_01.jpg' alt='fruits' className='w-100 h-100 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-lg-6 col-12'>
                        <div className='row'>
                            <div className='col-6'>
                                <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/e1a00954-68f4-4295-a01a-2319ef66f039/hp_fresh-fruits-fnvStorefront_m_250822_275x184_02.jpg' alt='fruits' className='w-100 h-100 border border-secondary border-opacity-10' />
                            </div>
                            <div className='col-6'>
                                <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/e1a00954-68f4-4295-a01a-2319ef66f039/hp_fresh-veggs-fnvStorefront_m_250822_275x184_03.jpg' alt='fruits' className='w-100 h-100 border border-secondary border-opacity-10' />
                            </div>
                            <div className='col-6'>
                                <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/e1a00954-68f4-4295-a01a-2319ef66f039/hp_cuts-sporouts-fnvStorefront_m_250822_275x184_04.jpg' alt='fruits' className='w-100 h-100 border border-secondary border-opacity-10' />
                            </div>
                            <div className='col-6'>
                                <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/e1a00954-68f4-4295-a01a-2319ef66f039/hp_exotic-fnv-fnvStorefront_m_250822_275x184_05.jpg' alt='fruits' className='w-100 h-100 border border-secondary border-opacity-10' />
                            </div>
                        </div>
                    </div>
                    <div className='col-12 mt-5'>
                        <h3 className='text-dark'>Your Daily Staples</h3>
                        <hr />
                    </div>
                    <div className='col-lg-2 col-md-4 col-6'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/5694e414-d3fa-4494-9bb7-420b6de9b95a/hp_atta-flour-staplesStorefront_m_480_250822_01.jpg' alt='staple' className='w-100 h-100 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-lg-2 col-md-4 col-6'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/5694e414-d3fa-4494-9bb7-420b6de9b95a/hp_rice-rice-products-staplesStorefront_m_480_250822_02.jpg' alt='staple' className='w-100 h-100 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-lg-2 col-md-4 col-6'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/5694e414-d3fa-4494-9bb7-420b6de9b95a/hp_dals-pules-staplesStorefront_m_480_250822_03.jpg' alt='staple' className='w-100 h-100 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-lg-2 col-md-4 col-6'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/5694e414-d3fa-4494-9bb7-420b6de9b95a/hp_coookinhg-oil-staplesStorefront_m_480_250822_04.jpg' alt='staple' className='w-100 h-100 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-lg-2 col-md-4 col-6'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/5694e414-d3fa-4494-9bb7-420b6de9b95a/hp_dry-fruits-staplesStorefront_m_480_250822_05.jpg' alt='staple' className='w-100 h-100 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-lg-2 col-md-4 col-6'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/5694e414-d3fa-4494-9bb7-420b6de9b95a/hp_salt-suger-staplesStorefront_m_480_250822_06.jpg' alt='staple' className='w-100 h-100 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-12 mt-5'>
                        <h3 className='text-dark'>Beverages</h3>
                        <hr />
                    </div>
                    <div className='col-lg-6 col-12'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/99ed1680-9fc5-488b-b97a-74071a73b3a5/hp_soft-drinks-beveragesStorefront_m_250822_560x378_01.jpg' alt='Beverages' className='w-100 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-lg-6 col-12'>
                        <div className='row'>
                            <div className='col-6'>
                                <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/99ed1680-9fc5-488b-b97a-74071a73b3a5/hp_juices-beveragesStorefront_m_250822_275x184_02.jpg' alt='Beverages' className='w-100 h-100 border border-secondary border-opacity-10' />
                            </div>
                            <div className='col-6'>
                                <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/99ed1680-9fc5-488b-b97a-74071a73b3a5/hp_health-drinks-beveragesStorefront_m_250822_275x184_03.jpg' alt='Beverages' className='w-100 h-100 border border-secondary border-opacity-10' />
                            </div>
                            <div className='col-6'>
                                <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/99ed1680-9fc5-488b-b97a-74071a73b3a5/hp_energy-drinks-beveragesStorefront_m_250822_275x184_04.jpg' alt='Beverages' className=' w-100 h-100 border border-secondary border-opacity-10' />
                            </div>
                            <div className='col-6'>
                                <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/99ed1680-9fc5-488b-b97a-74071a73b3a5/hp_coffee-tea-beveragesStorefront_m_250822_275x184_05.jpg' alt='Beverages' className='w-100 h-100 border border-secondary border-opacity-10' />
                            </div>
                        </div>
                    </div>
                    <div className='col-12 mt-5'>
                        <h3 className='text-dark'>Snacks Store</h3>
                        <hr />
                    </div>
                    <div className='col-lg-3  col-6'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/0cc0cfe7-fbc9-41c2-a1ed-8e4939fab712/hp_namkeens-chips-nachos-snacksStorefront_m_480_250822_01.jpg' alt='snacks' className='w-100 me-2 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-lg-3  col-6'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/0cc0cfe7-fbc9-41c2-a1ed-8e4939fab712/hp_monsoon-snacks-snacksStorefront_m_480_250822_02.jpg' alt='snacks' className='w-100 me-2 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-lg-3  col-6'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/0cc0cfe7-fbc9-41c2-a1ed-8e4939fab712/hp_soups-noodles-pasta-snacksStorefront_m_480_250822_03.jpg' alt='snacks' className='w-100 me-2 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-lg-3  col-6'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/0cc0cfe7-fbc9-41c2-a1ed-8e4939fab712/hp_biscuits-cookies-snacksStorefront_m_480_250822_04.jpg' alt='snacks' className='w-100 me-2 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-12 mt-5'>
                        <h3 className='text-dark'>Cleaning & Household</h3>
                        <hr />
                    </div>
                    <div className='col-lg-3 col-6'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/1877fefa-2666-45cd-94ae-124092c0ef28/hp_cleaner-disfectants-cleaningStorefront_m_480_250822_01.jpg' alt='HouseHolds' className='w-100 me-2 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-lg-3 col-6'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/1877fefa-2666-45cd-94ae-124092c0ef28/hp_detergents-favric-cleaningStorefront_m_480_250822_02.jpg' alt='HouseHolds' className='w-100 me-2 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-lg-3 col-6'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/1877fefa-2666-45cd-94ae-124092c0ef28/hp_kitchen-wipes-cleaningStorefront_m_480_250822_03.jpg' alt='HouseHolds' className='w-100 me-2 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-lg-3 col-6'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/1877fefa-2666-45cd-94ae-124092c0ef28/hp_fresheners-repellents-cleaningStorefront_m_480_250822_04.jpg' alt='HouseHolds' className='w-100 me-2 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-12 mt-5'>
                        <h3 className='text-dark'>Beauty & Hygiene</h3>
                        <hr />
                    </div>
                    <div className='col-lg-6 col-12'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/6d0206b0-a6e1-4c4e-9a2a-c8426acbeea5/t1_hp_b&h_c_makeup_378_250822.jpg' alt='Beauty' className='w-100 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-lg-6 col-12'>
                        <div className='row'>
                            <div className='col-6'>
                                <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/6d0206b0-a6e1-4c4e-9a2a-c8426acbeea5/t1_hp_b&h_c_min-30_184_250822.jpg' alt='Beauty' className='h-100 w-100 border border-secondary border-opacity-10' />
                            </div>
                            <div className='col-6'>
                                <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/6d0206b0-a6e1-4c4e-9a2a-c8426acbeea5/t1_hp_b&h_c_under-199_184_250822.jpg' alt='Beauty' className='h-100 w-100 border border-secondary border-opacity-10' />
                            </div>
                            <div className='col-6'>
                                <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/6d0206b0-a6e1-4c4e-9a2a-c8426acbeea5/t1_hp_b&h_c_scent_184_250822.jpg' alt='Beauty' className='h-100 w-100 border border-secondary border-opacity-10' />
                            </div>
                            <div className='col-6'>
                                <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/6d0206b0-a6e1-4c4e-9a2a-c8426acbeea5/t1_hp_b&h_c_shaving_184_250822.jpg' alt='Beauty' className='h-100 w-100 border border-secondary border-opacity-10' />
                            </div>
                        </div>
                    </div>

                    <div className='col-12 mt-5'>
                        <h3 className='text-dark'>Home & Kitchen Essentials</h3>
                        <hr />
                    </div>
                    <div className='col-lg-2 col-md-4 col-6'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/6d1ac41b-cf55-4460-9552-40fcceb1cc38/hp_under-990GMStorefront(Till10th)_m_250822_01.jpg' alt='Kitchen' className='w-100 h-100 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-lg-2 col-md-4 col-6'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/6d1ac41b-cf55-4460-9552-40fcceb1cc38/hp_100-199-GMStorefront(Till10th)_m_250822_02.jpg' alt='Kitchen' className='w-100 h-100 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-lg-2 col-md-4 col-6'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/6d1ac41b-cf55-4460-9552-40fcceb1cc38/hp_steel-utensils-GMStorefront(Till10th)_m_250822_03.jpg' alt='Kitchen' className='w-100 h-100 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-lg-2 col-md-4 col-6'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/6d1ac41b-cf55-4460-9552-40fcceb1cc38/hp_gas-stove-GMStorefront(Till10th)_m_250822_04.jpg' alt='Kitchen' className='w-100 h-100 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-lg-2 col-md-4 col-6'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/6d1ac41b-cf55-4460-9552-40fcceb1cc38/hp_bathware-GMStorefront(Till10th)_m_250822_05.jpg' alt='Kitchen' className='w-100 h-100 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-lg-2 col-md-4 col-6'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/6d1ac41b-cf55-4460-9552-40fcceb1cc38/hp_gardening-GMStorefront(Till10th)_m_250822_06.jpg' alt='Kitchen' className='w-100 h-100 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-12 mt-5 border border-secondary border-opacity-10'>
                        <div id="carouselExampleControls" className="carousel slide" data-bs-ride="carousel">
                            <div className="carousel-inner">
                                <div className="carousel-item active">
                                    <img src="https://www.bigbasket.com/media/uploads/banner_images/hp_m_babycare_250822_400.jpg" className="d-block w-100" alt="couroselpics" />
                                </div>
                                <div className="carousel-item">
                                    <img src="https://www.bigbasket.com/media/uploads/banner_images/hp_m_petstore_250822_400.jpg" className="d-block w-100" alt="couroselpics" />
                                </div>
                                <div className="carousel-item">
                                    <img src="https://www.bigbasket.com/media/uploads/banner_images/hp_bcd_m_bcd_250822_400.jpg" className="d-block w-100" alt="couroselpics" />
                                </div>
                                <div className="carousel-item">
                                    <img src="https://www.bigbasket.com/media/uploads/banner_images/hp_m_health_suppliment_250822_400.jpg" className="d-block w-100" alt="couroselpics" />
                                </div>
                            </div>
                            <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span className="visually-hidden">Previous</span>
                            </button>
                            <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                <span className="visually-hidden">Next</span>
                            </button>
                        </div>
                    </div>
                    <div className='col-12 mt-5'>
                        <h3 className='text-dark'>Brand Store</h3>
                        <hr />
                    </div>
                    <div className='col-lg-2 col-md-4 col-6'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/1690d585-9460-4c75-8e70-669f76d0a7f5/hp_amul-brandStorefront_m_480_250822_01.jpg' alt='Brands' className='w-100 h-100 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-lg-2 col-md-4 col-6'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/1690d585-9460-4c75-8e70-669f76d0a7f5/hp_dettol-brandStorefront_m_480_250822_02.jpg' alt='Brands' className='w-100 h-100 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-lg-2 col-md-4 col-6'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/1690d585-9460-4c75-8e70-669f76d0a7f5/hp_cocacola-brandStorefront_m_480_250822_03.jpg' alt='Brands' className='w-100 h-100 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-lg-2 col-md-4 col-6'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/1690d585-9460-4c75-8e70-669f76d0a7f5/hp_loreal-brandStorefront_m_480_250822_04.jpg' alt='Brands' className='w-100 h-100 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-lg-2 col-md-4 col-6'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/1690d585-9460-4c75-8e70-669f76d0a7f5/hp_india-gate-brandStorefront_m_480_250822_05.jpg' alt='Brands' className='w-100 h-100 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-lg-2 col-md-4 col-6'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/1690d585-9460-4c75-8e70-669f76d0a7f5/hp_durex-brandStorefront_m_480_250822_06.jpg' alt='Brands' className='w-100 h-100 border border-secondary border-opacity-10' />
                    </div>
                </div>
            </div>
        )
    }
}

export default MoreProducts
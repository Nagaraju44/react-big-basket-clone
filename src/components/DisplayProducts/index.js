import React, { Component } from 'react'
import initialState from '../../Redux/initState/initState'
import { Link } from 'react-router-dom'

export class DisplayProducts extends Component {
    render() {
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-12 text-start'>
                        <Link to='/'><i className="fa-solid fa-arrow-left mt-md-5 mt-3 mb-md-5 mb-3 fs-3 text-dark"></i></Link>
                    </div>
                    {initialState.ProductsData.map((product) => {
                        return <div className='col-lg-3 col-md-4 col-6 border border-secondary border-opacity-25 rounded pb-2 mb-2' key={product.id}>
                            <Link to={`/viewproduct/${product.id}`} className='text-decoration-none'>
                                <div>
                                    <div className='d-flex justify-content-end px-1 border-bottom border-light pt-1'>
                                        <p className='text-danger'>Get {product.discount}% off</p>
                                        <i className="bi bi-brightness-low-fill text-danger"></i>
                                    </div>
                                    <img src={product.img} className="w-75 h-25 " alt="item" />
                                    <div className='text-start'>
                                        <span className='text-secondary'>Fresho</span>
                                        <p className='text-dark'>{product.title}</p>
                                        <select className="form-select mb-2" aria-label="Default select example">
                                            <option>{product.quantity}-Rs.{product.price}</option>
                                        </select>
                                        <div className='pricing-container pb-2'>
                                            <p className='mb-0 fs-6 text-secondary'>MRP:<s className='fs-6'>Rs.{product.originalPrice}</s><span className='text-dark'>Rs.{product.price}</span></p>
                                            <p className='text-secondary text-wrap'><i className="fa-solid fa-truck ms-2 me-2"></i>Standard Deliver: 11 sep, 3PM to 7PM</p>
                                        </div>
                                        <div className='text-start mt-2 ms-3'>
                                            <button className='btn btn-danger btn-sm opacity-75'>View Product</button>
                                        </div>
                                    </div>
                                </div>
                            </Link>
                        </div>
                    })}

                </div>
            </div>
        )
    }
}


export default DisplayProducts